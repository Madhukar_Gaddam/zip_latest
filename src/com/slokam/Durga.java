package com.slokam;

import java.util.Scanner;

public class Durga {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        int t=sc.nextInt();

        for(int i=0;i<t;i++)
        {

            try
            {
                long x=sc.nextLong();
                System.out.println(x+" can be fitted in:");
                if(x>=-128 && x<=127)System.out.println("* byte");
                 if((short)Math.pow(-2, 15)<=x &&x<=((short)Math.pow(2, 15))-1) {
                        System.out.println("* short");
                    }
                 if((int)Math.pow(-2, 31)<=x &&x<=(int)(Math.pow(2, 31))) {
                        System.out.println("* int");
                    }
                 if((long)Math.pow(-2, 63)<=x &&x<=(long)(Math.pow(2, 63))) {
                        System.out.println("* long");
                    }
                    //complete the code
            }
            catch(Exception e)
            {
                System.out.println(sc.next()+" can't be fitted anywhere.");
            }

        }
	}

}
